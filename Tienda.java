public class Tienda {
    public static void main(String[] args) {
        Portatil dell = new Portatil("dell");
        

        dell.setCodigo("23433");
        dell.setPrecio(31499.00);
        dell.setPeso(2.5);

        System.out.println("Codigo: "+dell.getCodigo());
        System.out.println("Precio: "+dell.getPrecio());
        System.out.println("Peso: "+dell.getPeso());
        
        
        Sobremesa lenovo = new Sobremesa("lenovo");

        lenovo.setCodigo("245753");
        lenovo.setPrecio(29998.99);
        lenovo.setTorre("Estructura Geometrica crea armonia y estabilidad, dando lugar al ideal de la pureza y el sentido de la creatividad");

        System.out.println("Codigo: "+lenovo.getCodigo());
        System.out.println("Precio: "+lenovo.getPrecio());
        System.out.println("Descripcion de torre: "+lenovo.getTorre());

    }
}
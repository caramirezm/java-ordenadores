public class Ordenadores {
    private String codigo;
    private Double precio;
    public void setPrecio (Double precio){
        this.precio = precio;
    }

    public Double getPrecio(){
        return this.precio;
    }

    public void setCodigo (String codigo){
        this.codigo = codigo;
    }

    public String getCodigo(){
        return this.codigo;
    }

    public static void main(String[] args) {
        System.out.println("Bienvenido a la tienda");
    }
}


class Portatil extends Ordenadores{
    public Portatil(String lap){
        System.out.println("portatil "+lap+",Ideal para sus viajes");
        
    }

    private double peso;
    public void setPeso(double Kilos){
        this.peso=Kilos;
    }

    public double getPeso(){
        return this.peso;
    }

}

class Sobremesa extends Ordenadores{
    public Sobremesa(String compu){
        System.out.println("Ordenador de Sobremesa: "+compu+",Es el que mas pesa, pero el que menos cuesta");
    }

    private String torre;
    public void setTorre(String tipo){
        this.torre=tipo;
    }

    public String getTorre(){
        return this.torre;
    }
}